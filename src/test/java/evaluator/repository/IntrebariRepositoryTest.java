package evaluator.repository;

import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Costi on 02.04.2018.
 */
public class IntrebariRepositoryTest {

    private IntrebariRepository intrebariRepository;

    @Before
    public void setUp() throws Exception {
        intrebariRepository = new IntrebariRepository();
    }

    //varianta corecta nu este 1,2,3
    @Test
    public void addIntrebare() throws Exception {
        try{
            new Intrebare("Dsada?","1)ceva","2)sa","3)cea","ds","Info");

            assertFalse(true);
        }
        catch (InputValidationFailedException ex){
            assertTrue(true);
        }

    }

    //adaugare cu success
    @Test
    public void addIntrebare1() throws Exception {
        try{
            new Intrebare("Dsada?","1)ceva","2)sa","3)cea","2","Info");
            assertTrue(true);
        }
        catch (InputValidationFailedException ex){
            assertFalse(true);
        }
    }

    //enuntul este vid
    @Test
    public void addIntrebare2() throws Exception {
        try{
            new Intrebare("","1)ceva","2)sa","3)cea","ds","Info");

            assertFalse(true);
        }
        catch (InputValidationFailedException ex){
            assertTrue(true);
        }
    }

    //domeniul este vid
    @Test
    public void addIntrebare3() throws Exception {
        try{
            new Intrebare("Ce faci?","1)ceva","2)sa","3)cea","2","");

            assertFalse(true);
        }
        catch (InputValidationFailedException ex){
            assertTrue(true);
        }
    }

    //prima litera din enunt nu e majuscula
    @Test
    public void addIntrebare4() throws Exception {
        try{
            new Intrebare("ce faci?","1)ceva","2)sa","3)cea","2","Zaza");

            assertFalse(true);
        }
        catch (InputValidationFailedException ex){
            assertTrue(true);
        }
    }

    //lungimea enuntului depaseste 100 caractere
    @Test
    public void addIntrebare5() throws Exception {
        try{
            new Intrebare("Ce facidsadasdasdasdasdsadpsakdasjdasoijdasijdoiasjdoisajdiojasodjasdjasiodjaoisjdaoisjdoiasjdoiajsdoiajsoidjasdasdojasdoijsaoidjasdoijasdiojasoidjasdjsaiodja?","1)ceva","2)sa","3)cea","2","Lada");

            assertFalse(true);
        }
        catch (InputValidationFailedException ex){
            assertTrue(true);
        }
    }

    //fix 100 caractere
    @Test
    public void baddIntrebare6() throws Exception {
        try{
            new Intrebare("Th7b1XbXQUWHEHRQdkWwiJ0QdiK3KSYA3faXRKrTk4R7MMIgWpSxZJoZhKxy9ktBBV88GAiaxoAph3wO2i7pOd1tfTbPmsOZmXv?","1)ceva","2)sa","3)cea","2","Dada");
            assertTrue(true);

        }
        catch (InputValidationFailedException ex){
            assertFalse(true);

        }
    }

    //101 caractere
    @Test
    public void addIntrebare7() throws Exception {
        try{
            new Intrebare("Th7b1XbXQUWHEHRQdkWwiJ0QdiK3KSYA3faXRKrTk4R7MMIgWpSxZJoZhKxy9ktBBV88GAiaxoAph3wO2i7pOd1tfTbPmsOZmXsv?","1)ceva","2)sa","3)cea","2","Dada");
            assertFalse(true);
        }
        catch (InputValidationFailedException ex){
            assertTrue(true);
        }
    }

    // lungimea domeniului depaseste 30 caractere
    @Test
    public void addIntrebare8() throws Exception {
        try{
            new Intrebare("Da?","1)ceva","2)sa","3)cea","2","Th7b1XbXQUWHEHRQdkWwiJ0QdiK3KSYA3faXRKrTk4R7MMIgWpSxZJoZhKxy9ktBBV88GAiaxoAph3wO2i7pOd1tfTbPmsOZmXsv");
            assertFalse(true);
        }
        catch (InputValidationFailedException ex){
            assertTrue(true);
        }
    }

    //ultimul caracter din enunt nu e "?"
    @Test
    public void addIntrebare9() throws Exception {
        try{
            new Intrebare("Dadsada","1)ceva","2)sa","3)cea","2","Th7b1Xb");
            assertFalse(true);
        }
        catch (InputValidationFailedException ex){
            assertTrue(true);
        }
    }

    //varianta 2 e vida
    @Test
    public void addIntrebare10() throws Exception {
        try{
            new Intrebare("Dadsada?","1)ceva","","3)cea","2","Th7b1X");
            assertFalse(true);
        }
        catch (InputValidationFailedException ex){
            assertTrue(true);
        }
    }

    //varianta 3 e vida
    @Test
    public void addIntrebare11() throws Exception {
        try{
            new Intrebare("Dadsada?","1)ceva","2)dsa","","2","Th7b1X");
            assertFalse(true);
        }
        catch (InputValidationFailedException ex){
            assertTrue(true);
        }
    }

    //varianta 1 e vida
    @Test
    public void addIntrebare12() throws Exception {
        try{
            new Intrebare("Dadsada?","","2)dsa","3)sdz","2","Th7b1X");
            assertFalse(true);
        }
        catch (InputValidationFailedException ex){
            assertTrue(true);
        }
    }



}