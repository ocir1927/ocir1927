package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Costi on 07.05.2018.
 */
public class TopDownIntegration {
    private IntrebariRepository intrebariRepository;
    private AppController appController;

    @Before
    public void setUp() throws Exception {
        intrebariRepository = new IntrebariRepository();
        appController = new AppController();
    }

    @Test
    public void unitA() throws Exception {
        try{
            new Intrebare("","1)ceva","2)sa","3)cea","ds","Info");

            assertFalse(true);
        }
        catch (InputValidationFailedException ex){
            assertTrue(true);
        }
    }

    @Test
    public void integrationAB() throws Exception {
        try {
            appController.addNewIntrebare(new Intrebare("Dsa?", "1)123", "2)dsa", "3)dsa", "2", "Eng"));
            appController.addNewIntrebare(new Intrebare("Dsa1?", "1)123", "2)dsa", "3)dsa", "2", "Info"));
            appController.addNewIntrebare(new Intrebare("Dsa2?", "1)123", "2)dsa", "3)dsa", "2", "Mate"));
            appController.addNewIntrebare(new Intrebare("Dsa3?", "1)123", "2)dsa", "3)dsa", "2", "Info"));
            appController.addNewIntrebare(new Intrebare("Dsa4?", "1)123", "2)dsa", "3)dsa", "2", "Ro"));
            appController.addNewIntrebare(new Intrebare("Dsa5?", "1)123", "2)dsa", "3)dsa", "2", "Info"));
            appController.addNewIntrebare(new Intrebare("Dsa6?", "1)123", "2)dsa", "3)dsa", "2", "Gegra"));

            appController.createNewTest();
            assertTrue(true);
        }
        catch (Exception ex){
            assertFalse(true);
        }

    }

    @Test
    public void integrationABC() throws Exception {
        try {
            appController.addNewIntrebare(new Intrebare("Dsa?", "1)123", "2)dsa", "3)dsa", "2", "Eng"));
            appController.addNewIntrebare(new Intrebare("Dsa1?", "1)123", "2)dsa", "3)dsa", "2", "Info"));
            appController.addNewIntrebare(new Intrebare("Dsa2?", "1)123", "2)dsa", "3)dsa", "2", "Mate"));
            appController.addNewIntrebare(new Intrebare("Dsa3?", "1)123", "2)dsa", "3)dsa", "2", "Info"));
            appController.addNewIntrebare(new Intrebare("Dsa4?", "1)123", "2)dsa", "3)dsa", "2", "Ro"));
            appController.addNewIntrebare(new Intrebare("Dsa5?", "1)123", "2)dsa", "3)dsa", "2", "Info"));
            appController.addNewIntrebare(new Intrebare("Dsa6?", "1)123", "2)dsa", "3)dsa", "2", "Gegra"));

            appController.createNewTest();

            appController.getStatistica();

            assertTrue(true);
        }
        catch (Exception ex){
            assertFalse(true);
        }

    }

}
