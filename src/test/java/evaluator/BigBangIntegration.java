package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Costi on 06.05.2018.
 */
public class BigBangIntegration {
    private IntrebariRepository intrebariRepository;
    private AppController appController;

    @Before
    public void setUp() throws Exception {
        intrebariRepository = new IntrebariRepository();
        appController = new AppController();
    }

    //adaugare cu success
    @Test
    public void addIntrebare1() throws Exception {
        try{
            new Intrebare("Dsada?","1)ceva","2)sa","3)cea","2","Info");
            assertTrue(true);
        }
        catch (InputValidationFailedException ex){
            assertFalse(true);
        }
    }

    @Test
    public void createNewTestP02() throws Exception {
        try {
            appController = new AppController();
            appController.addNewIntrebare(new Intrebare("Dsa?","1)123","2)dsa","3)dsa","2","Info"));
            appController.addNewIntrebare(new Intrebare("Dsa1?","1)123","2)dsa","3)dsa","2","Info"));
            appController.addNewIntrebare(new Intrebare("Dsa2?","1)123","2)dsa","3)dsa","2","Mate"));
            appController.addNewIntrebare(new Intrebare("Dsa3?","1)123","2)dsa","3)dsa","2","Info"));
            appController.addNewIntrebare(new Intrebare("Dsa4?","1)123","2)dsa","3)dsa","2","Ro"));
            appController.addNewIntrebare(new Intrebare("Dsa5?","1)123","2)dsa","3)dsa","2","Info"));
            appController.addNewIntrebare(new Intrebare("Dsa6?","1)123","2)dsa","3)dsa","2","Info"));
            appController.createNewTest();
            assertFalse(true);
        }catch (NotAbleToCreateTestException ex){
            assertTrue(true);
        }
    }

    @Test
    public void testStatistica1() throws Exception {
        appController = new AppController();
        try{
            appController.getStatistica();
            assertFalse(true);
        }
        catch (NotAbleToCreateStatisticsException ex){
            assertTrue(true);
        }
    }

    @Test
    public void bigBangIntegration() throws Exception {
        try {
            appController.addNewIntrebare(new Intrebare("Dsa?", "1)123", "2)dsa", "3)dsa", "2", "Eng"));
            appController.addNewIntrebare(new Intrebare("Dsa1?", "1)123", "2)dsa", "3)dsa", "2", "Info"));
            appController.addNewIntrebare(new Intrebare("Dsa2?", "1)123", "2)dsa", "3)dsa", "2", "Mate"));
            appController.addNewIntrebare(new Intrebare("Dsa3?", "1)123", "2)dsa", "3)dsa", "2", "Info"));
            appController.addNewIntrebare(new Intrebare("Dsa4?", "1)123", "2)dsa", "3)dsa", "2", "Ro"));
            appController.addNewIntrebare(new Intrebare("Dsa5?", "1)123", "2)dsa", "3)dsa", "2", "Info"));
            appController.addNewIntrebare(new Intrebare("Dsa6?", "1)123", "2)dsa", "3)dsa", "2", "Gegra"));

            appController.createNewTest();

            appController.getStatistica();
            assertTrue(true);
        }
        catch (Exception ex){
            assertFalse(true);
        }

    }






}
