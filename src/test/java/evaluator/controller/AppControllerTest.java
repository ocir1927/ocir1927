package evaluator.controller;

import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by Costi on 23.04.2018.
 */
public class AppControllerTest {
    private IntrebariRepository intrebariRepository;
    private AppController appController;

    @Before
    public void setUp() throws Exception {
        intrebariRepository = new IntrebariRepository();
        appController = new AppController();
    }

    @Test
    public void createNewTestP01() throws Exception {
        try {
            appController = new AppController();
            appController.createNewTest();
            assertFalse(true);
        }catch (NotAbleToCreateTestException ex){
            assertTrue(true);
        }
    }
    @Test
    public void bcreateNewTestP02() throws Exception {
        try {
            appController = new AppController();
            appController.addNewIntrebare(new Intrebare("Dsa?","1)123","2)dsa","3)dsa","2","Info"));
            appController.addNewIntrebare(new Intrebare("Dsa1?","1)123","2)dsa","3)dsa","2","Info"));
            appController.addNewIntrebare(new Intrebare("Dsa2?","1)123","2)dsa","3)dsa","2","Mate"));
            appController.addNewIntrebare(new Intrebare("Dsa3?","1)123","2)dsa","3)dsa","2","Info"));
            appController.addNewIntrebare(new Intrebare("Dsa4?","1)123","2)dsa","3)dsa","2","Ro"));
            appController.addNewIntrebare(new Intrebare("Dsa5?","1)123","2)dsa","3)dsa","2","Info"));
            appController.addNewIntrebare(new Intrebare("Dsa6?","1)123","2)dsa","3)dsa","2","Info"));
            appController.createNewTest();
            assertFalse(true);
        }catch (NotAbleToCreateTestException ex){
            assertTrue(true);
        }
    }

    @Test
    public void createNewTestP03() throws Exception {
        appController = new AppController();
        appController.loadIntrebariFromFile("intrebari.txt");
        appController.createNewTest();
        assertTrue(true);
    }


    @Test
    public void testStatistica1() throws Exception {
        appController = new AppController();
        try{
            appController.getStatistica();
            assertFalse(true);
        }
        catch (NotAbleToCreateStatisticsException ex){
            assertTrue(true);
        }
    }

    @Test
    public void testStatistica2() throws Exception {
        appController = new AppController();
        appController.loadIntrebariFromFile("intrebari.txt");
        try {
            appController.getStatistica();
            assertTrue(true);
        }
        catch (Exception ex){
            assertFalse(true);
        }
    }

}