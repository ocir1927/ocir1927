package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Test;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

    private static final String file = "intrebari.txt";

    public static void main(String[] args) throws IOException {

        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

        AppController appController = new AppController();

        boolean activ = true;
        String optiune = null;

        while (activ) {

            System.out.println("");
            System.out.println("1.Adauga intrebare");
            System.out.println("2.Creeaza test");
            System.out.println("3.Statistica");
            System.out.println("4.Exit");
            System.out.println("");
            appController.loadIntrebariFromFile(file);


            optiune = console.readLine();

            switch (optiune) {
                case "1":
                    System.out.println("Adaugare intrebare noua...");
                    Intrebare intrebare = new Intrebare();
                    System.out.println("Enunt:");
                    intrebare.setEnunt(console.readLine());
                    System.out.println("raspuns 1:");
                    intrebare.setVarianta1(console.readLine());
                    System.out.println("raspuns 2:");
                    intrebare.setVarianta2(console.readLine());
                    System.out.println("raspuns 3:");
                    intrebare.setVarianta3(console.readLine());
                    System.out.println("raspuns corect:");
                    intrebare.setVariantaCorecta(console.readLine());
                    System.out.println("domeniul");
                    intrebare.setDomeniu(console.readLine());


                    try {
                        appController.addNewIntrebare(intrebare);
                        appController.saveIntrebari(file);

                    } catch (DuplicateIntrebareException e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "2":
                    System.out.println("Creare test...");
                    try {
                        Test test = appController.createNewTest();
                        for (Intrebare intrebare1 : test.getIntrebari()) {
                            System.out.println(intrebare1.toString());
                        }
                    } catch (NotAbleToCreateTestException e) {
                        System.out.println(e.getMessage());
                    }


                    break;
                case "3":
                    Statistica statistica;
                    try {
                        statistica = appController.getStatistica();
                        System.out.println(statistica);
                    } catch (NotAbleToCreateStatisticsException e) {
                        System.out.println("Nu s-a putut crea statistica");
                    }

                    break;
                case "4":
                    activ = false;
                    break;
                default:
                    break;
            }
        }

    }

}
